<?php include("navbar.php");
/**
 * Created by PhpStorm.
 * Permission: DeskTop
 * Date: 3/9/2018
 * Time: 10:51 PM
 */
?>
<html>
<head>
    <script>
        function Main() {
            //      alert("Permissions Mangement ");
            var permissionName = document.getElementById("permissionName");
            var description = document.getElementById("description");

            var allPermissions;// = SecurityManager.GetAllPermissions();

            var isPermissionNameVerify = true;
            var permissionData = {};

            permissionName.onfocusout = checkPermissionName;


            tablefill();

            function tablefill() {
                $("#permissionTable").html('');

                var setting = {};
                setting.type = "Post";
                setting.url = "api.php";
                setting.dataType = "json";
                setting.data = {"act": "permission"};
                setting.success = function (permissions) {
                    allPermissions = permissions;
                    for (permission in permissions) {

                        var row = $("<tr>");
                        $("#permissionTable").append(row);

                        row.append($("<td>").text(permissions[permission]["permissionid"]));
                        row.append($("<td>").text(permissions[permission]["name"]));
                        row.append($("<td>").text(permissions[permission]["description"]));

                        var datainrow = $("<td>");
                        row.append(datainrow.html("<a href='#' >edit</a>"));

                        datainrow.attr("onclick", "editPermission(" + permissions[permission]["permissionid"] + ");")

                        var datainrow = $("<td>");
                        row.append(datainrow.html("<a href='#' >delete</a>"));

                        datainrow.attr("onclick", "deletePermission(" + permissions[permission]["permissionid"] + ");")
                    }
                };
                setting.error = function () {
                    alert("permission error");
                };
                $.ajax(setting);
            }


            function checkPermissionName() {
                if ($("#permissionName").val().trim(' ') == 0) {
                    //       alert("please  Enter Permission Name");
                    $("#permissionName").css("border", "1px solid red");
                    isPermissionNameVerify = false;
                }

                else {
                    $("#permissionName").css("border", "1px solid black");
                    isPermissionNameVerify = true;
                }
                var f = false;
                for (permission in allPermissions) {
                    if ($("#permissionName").val() == allPermissions[permission]["login"] && allPermissions[permission]["id"] != permissionData["ID"])
                        f = true;
                }
                if (f) {
                    $("#permissionName").css("border", "1px solid red");
                    isPermissionNameVerify = false;
                    alert("Permission Name already  exists");
                }
            }


            $("#savebtn").on("click", function () {

                checkPermissionName();
                if (isPermissionNameVerify) {
                    permissionData["createdby"] = $("#adminid").val();
                    permissionData["permissionName"] = $("#permissionName").val();
                    permissionData["description"] = $("#description").val();

                    permissionData["act"] = "savePermission";
                    var setting = {};
                    setting.type = "Post";
                    setting.url = "api.php";
                    setting.dataType = "json";
                    setting.data = permissionData;
                    setting.success = function (u) {
                        alert("Permission Saved");
                        $(':input').val('');
                        tablefill();
                    };
                    setting.error = function () {
                        alert("Save permission error");

                    }
                    $.ajax(setting);
                } else alert("Some value are missing");

            });
            editPermission = function (permissionId) {
                permissionData = SecurityManager.GetPermissionById(permissionId);
                permissionName.value = permissionData["permissionName"];
                description.value = permissionData["description"];

            }

            deletePermission = function (permissionId) {
                var isdelete = confirm("Are You sure  You want  to delete Permission " );
                if (isdelete) {
                    var setting = {};
                    setting.type = "Post";
                    setting.url = "api.php";
                    setting.dataType = "json";
                    setting.data = {"act": "deletePermission", "id": permissionId};
                    setting.success = function (r) {
                        tablefill();
                    }
                    setting.error = function () {
                        alert("Delete error");
                    }
                    $.ajax(setting);
                }
            }

        }


    </script>
</head>
<body onload="Main();">
<div class="container-fluid">
    <div class="row form-group col-lg-4 col-lg-offset-2">
        <form>
            <legend align="center">Permissions</legend>
            <input hidden name="adminid" id="adminid" value="<?php echo $adminid ?>">
            <div class="form-group">
                <label>Permission Name:*</label>
                <input type="text" id="permissionName" class="form-control">
            </div>
            <div class="form-group"><label>Description:</label>
                <input type="text" id="description" class="form-control">
            </div>
            <div class="form-group">
                <input type="button" id="savebtn" value="Save" class="btn  btn-success col-lg-6">
                <input type="reset" value="Clear" class="btn  btn-warning col-lg-6">
            </div>
        </form>
    </div>
    <div class="col-lg-5 col-lg-offset-1">

        <table class="table" >
            <legend align="center">Permission Table</legend>
            <tr>
                <th>ID</th>
                <th>Permission Name</th>
                <th>Description</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
            <tbody id=""permissionTable""  ></tbodyid>
        </table>
    </div>
</div>
</body>
</html>

