<?php require ('navbar.php');
require ('conn.php');
/**
 * Created by PhpStorm.
 * User: DeskTop
 * Date: 3/24/2018
 * Time: 8:23 PM
 */
?>

<html>
<body>
<div class="col-lg-8 col-lg-offset-1">

    <table class="table" >
        <legend align="center">Login History</legend>
        <tr>
            <th>ID</th>
            <th>userID</th>
            <th>User Name</th>
            <th>Login Time</th>
            <th>Machine IP</th>

        </tr>
        <?php
        $sql = "SELECT *from loginhistory";
        //Step-2: Execute SQL Query
        $result = mysqli_query($conn, $sql);

        //Step-3: Get count of result
        $recordsFound = mysqli_num_rows($result);
        if ($recordsFound > 0) {

            //Step-4: Iterate row by row
            while ($row = mysqli_fetch_assoc($result)) {
                $id = $row["id"];
                $uid = $row["userid"];
                $userName = $row["login"];
                $logintime = $row["logintime"];
                $machineip = $row["machinip"];

                //Step-5: Display values
                echo "<tr><td>$id</td><td>$uid</td><td>$userName</td><td>$logintime</td>";
                echo "<td>$machineip</td>";
                echo "</tr>";
            }
        }
        ?>
    </table>
</div>
</body>
</html>
