<?php include("navbar.php");
/**
 * Created by PhpStorm.
 * Role: DeskTop
 * Date: 3/9/2018
 * Time: 10:51 PM
 */
?>
<html>
<head>
    <script>
        function Main() {
            //      alert("Roles Mangement ");
            var roleName = document.getElementById("roleName");
            var description = document.getElementById("description");

            var allRoles;// = SecurityManager.GetAllRoles();

            var isRoleNameVerify = true;
            var roleData = {};

            roleName.onfocusout = checkRoleName;


            tablefill();

            function tablefill() {
                $("#roleTable").html('');

                var setting = {};
                setting.type = "Post";
                setting.url = "api.php";
                setting.dataType = "json";
                setting.data = {"act": "role"};
                setting.success = function (roles) {
                    allRoles = roles;
                    for (role in roles) {

                        var row = $("<tr>");
                        $("#roleTable").append(row);

                        row.append($("<td>").text(roles[role]["roleid"]));
                        row.append($("<td>").text(roles[role]["name"]));
                        row.append($("<td>").text(roles[role]["description"]));

                        var datainrow = $("<td>");
                        row.append(datainrow.html("<a href='#' >edit</a>"));

                        datainrow.attr("onclick", "editRole(" + roles[role]["roleid"] + ");")

                        var datainrow = $("<td>");
                        row.append(datainrow.html("<a href='#' >delete</a>"));

                        datainrow.attr("onclick", "deleteRole(" + roles[role]["roleid"] + ");")
                    }
                };
                setting.error = function () {
                    alert("role error");
                };
                $.ajax(setting);
            }


            function checkRoleName() {
                if ($("#roleName").val().trim(' ') == 0) {
                    //       alert("please  Enter Role Name");
                    $("#roleName").css("border", "1px solid red");
                    isRoleNameVerify = false;
                }
                else if ($("#roleName").val().indexOf(' ') >= 0) {
                    //         alert("Enter Role Name without spaces");
                    $("#roleName").css("border", "1px solid red");
                    isRoleNameVerify = false;
                }
                else {
                    $("#roleName").css("border", "1px solid black");
                    isRoleNameVerify = true;
                }
                var f = false;
                for (role in allRoles) {
                    if ($("#roleName").val() == allRoles[role]["login"] && allRoles[role]["id"] != roleData["ID"])
                        f = true;
                }
                if (f) {
                    $("#roleName").css("border", "1px solid red");
                    isRoleNameVerify = false;
                    alert("Role Name already  exists");
                }
            }


            $("#savebtn").on("click", function () {

                checkRoleName();
                if (isRoleNameVerify) {
                    roleData["createdby"] = $("#adminid").val();
                    roleData["roleName"] = $("#roleName").val();
                    roleData["description"] = $("#description").val();

                    roleData["act"] = "saveRole";
                    var setting = {};
                    setting.type = "Post";
                    setting.url = "api.php";
                    setting.dataType = "json";
                    setting.data = roleData;
                    setting.success = function (u) {
                        alert("Role Saved");
                        $(':input').val('');
                        tablefill();
                    };
                    setting.error = function () {
                        alert("Save role error");

                    }
                    $.ajax(setting);
                } else alert("Some value are missing");

            });
            editRole = function (roleId) {
                roleData = SecurityManager.GetRoleById(roleId);
                roleName.value = roleData["roleName"];
                description.value = roleData["description"];

            }
           
            deleteRole = function (roleId) {
                var isdelete = confirm("Are You sure  You want  to delete Role " );
                if (isdelete) {
                    var setting = {};
                    setting.type = "Post";
                    setting.url = "api.php";
                    setting.dataType = "json";
                    setting.data = {"act": "deleteRole", "id": roleId};
                    setting.success = function (r) {
                        tablefill();
                    }
                    setting.error = function () {
                        alert("Delete error");
                    }
                    $.ajax(setting);
                }
            }

        }


    </script>
</head>
<body onload="Main();">
<div class="container-fluid">
    <div class="row form-group col-lg-4 col-lg-offset-2">
        <form>
            <legend align="center">Roles</legend>
            <input hidden name="adminid" id="adminid" value="<?php echo $adminid ?>">
            <div class="form-group">
                <label>Role Name:*</label>
                <input type="text" id="roleName" class="form-control">
            </div>
            <div class="form-group"><label>Description:</label>
                <input type="text" id="description" class="form-control">
            </div>
            <div class="form-group">
                <input type="button" id="savebtn" value="Save" class="btn  btn-success col-lg-6">
                <input type="reset" value="Clear" class="btn  btn-warning col-lg-6">
            </div>
        </form>
    </div>
    <div class="col-lg-5 col-lg-offset-1">

        <table class="table" >
            <legend align="center">Role Table</legend>
            <tr>
                <th>ID</th>
                <th>Role Name</th>
                <th>Description</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
            <tbody id="roleTable"></tbody>
        </table>
    </div>
</div>
</body>
</html>

