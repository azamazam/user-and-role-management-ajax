<?php
/**
 * Created by PhpStorm.
 * User: DeskTop
 * Date: 3/10/2018
 * Time: 9:45 PM
 */ ?>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <script src="bootstrap/js/jquery.js" type="text/javascript"></script>
    <script src='bootstrap/js/bootstrap.min.js'></script>
    <script src='bootstrap/js/jquery.min.js'></script>

    <script src="SecurityManager.js"></script>
    <title>Assignment 1</title>

    <script>
        function Main() {
            var h3 = document.getElementById("welcome");
            var roleol = document.getElementById("roles");

            var userid = localStorage["userid"];
            //    localStorage["userid"] = "";

         //   alert(userid);
            var user = SecurityManager.GetUserById(userid);
            //   alert(JSON.stringify(user));
          //  debugger;

            h3.innerHTML = "Welcome <b>" + user.userName + "</b";
            var roles = SecurityManager.GetAllUserRoles();// GetUserRoleById(userid);
            var permission = SecurityManager.GetAllRolePermissions();

        //    alert(JSON.stringify(roles));
        //    alert(JSON.stringify(permission));

            for (role  in roles) {
                if (user.userName == roles[role].userName) {//    alert();
                    var roleli = document.createElement("li");
                    roleli.innerText = "Role :" + roles[role].roleName;

                    var permul = document.createElement("ul");
                    permul.innerHTML = "<b>Permissions</b>";

                    for (p in permission) {
                        var permli = document.createElement("li");
                        if (roles[role].roleName == permission[p].roleName)
                        {
                            permli.innerText = permission[p].permissionName;
                            permul.appendChild(permli);
                        }
                    }
                    roleli.appendChild(permul);
                    roleol.appendChild(roleli);

                }
            }


        }
    </script>
</head>
<body onload="Main()">

<h1 id='name'> Security Manager </h1>
<nav style="  border-top: 6px solid black; " class='navbar  navbar-default navbar-responsive'>
    <div class="container-fluid">
        <ul class="nav navbar-nav ">
            <li class="active">
                <a href="userpage.php" class="">Home</a>
            </li>
            <li><a href="index.php?logout=true">logout</a></li>
        </ul>
    </div>
</nav>
<h3 align="center" id="welcome"></h3>
<h4 class="panel-heading">Roles</h4>
<ol id="roles"></ol>

</body>

</html>