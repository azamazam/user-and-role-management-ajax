<?php include('navbar.php');
$id="";

/**
 *
 * Created by PhpStorm.
 * User: DeskTop
 * Date: 3/6/2018
 * Time: 7:59 PM
 */
?>
<html>
<head>
    <script>
        function Main() {

            var isUserNameVerify = true;
            var isNameVerify = true;
            var isEmailVerify = true;
            var isPasswordVerify = true;
            var isCountryVerify = true;
            var isCityVerify = true;

            $("#userName").on("focusout", checkUserName);
            $("#Name").on("focusout", checkName);
            $("#email").on("focusout", checkEmail);
            $("#password").on("focusout", checkPassword);
            $("#confirmPassword").on("focusout", matchPassword);
            $("#cmbCountries").on('change', fillCity);
            $("#cmbCities").on("change", checkCity);

            var userData = {};
            var allUsers;
            tablefill();
            fillcountry();

            function tablefill() {
                $("#userTable").html('');

                var setting = {};
                setting.type = "Post";
                setting.url = "api.php";
                setting.dataType = "json";
                setting.data = {"act": "user"};
                setting.success = function (users) {
                    allUsers = users;
                    for (user in users) {

                        var row = $("<tr>");
                        $("#userTable").append(row);

                        row.append($("<td>").text(users[user]["id"]));
                        row.append($("<td>").text(users[user]["login"]));
                        row.append($("<td>").text(users[user]["name"]));
                        row.append($("<td>").text(users[user]["email"]));

                        var datainrow = $("<td>");
                        row.append(datainrow.html("<a href='#' >edit</a>"));

                        datainrow.attr("onclick", "editUser(" + users[user]["id"] + ");")

                        var datainrow = $("<td>");
                        row.append(datainrow.html("<a href='#' >delete</a>"));

                        datainrow.attr("onclick", "deleteUser(" + users[user]["id"] + ");")
                    }
                };
                setting.error = function () {
                    alert("user error");
                };
                $.ajax(setting);
            }

            function fillcountry(countryid) {
                $("#cmbCountries").html(' <option value="0">--select--</option>');
                var setting = {};
                setting.type = "Post";
                setting.url = "api.php";
                setting.dataType = "json";
                setting.data = {"act": "country"};
                setting.success = function (countries) {

                    for (i = 0; i < countries.length; i++) {

                        $('#cmbCountries').append($('<option>',
                            {
                                value: countries[i].id,
                                text: countries[i].name
                            }));
                    }
                };
                setting.error = function () {
                    alert("error");
                };
                $.ajax(setting);
            }


                function fillCity(cityid) {

                //Remove all child elements (e.g. options)
                $("#cmbCities").html(' <option value="0">--select--</option>');
                var setting = {};
                setting.type = "Post";
                setting.url = "api.php";
                setting.dataType = "json";
                setting.data = {"act": "city", "id": $("#cmbCountries").val()};
                setting.success = function (cities) {

                    for (i = 0; i < cities.length; i++) {
                        $('#cmbCities').append($('<option>',
                            {
                                value: cities[i].id,
                                text: cities[i].name

                            }));
                    }
                };
                setting.error = function () {
                    alert("error");
                }
                $.ajax(setting);
            }


            editUser = function (userId) {

                $("#userName").val(userData["userName"]);
                $("#name").val(userData["name"]);
                $("#email").val(userData["email"]);
                $("#password").val(userData["password"]);
                $("#confirmPassword").val(userData["password"]);

                $("#cmbCities").html(' <option value="0"> --select--</option>');

                for (var i = 0; i < cities.length; i++) {
                    var opt = $("<option>").val(cities[i].CityID).text(cities[i].Name);
                    if (userData["city"] == cities[i].CityID)
                        opt.attr("selected", "selected");
                    $("#cmbCities").append(opt);
                }
            }

            deleteUser = function (userId) {
                var isdelete = confirm("Are You sure  You want  to delete User " );
                if (isdelete) {
                    var setting = {};
                    setting.type = "Post";
                    setting.url = "api.php";
                    setting.dataType = "json";
                    setting.data = {"act": "deleteUser", "id": userId};
                    setting.success = function (cities) {
                        tablefill();
                    }
                    setting.error = function () {
                        alert("Delete error");
                    }
                    $.ajax(setting);
                }
            }
            function checkUserName() {
                if ($("#userName").val().trim(' ') == 0) {
                    //       alert("please  Enter User Name");
                    $("#userName").css("border", "1px solid red");
                    isUserNameVerify = false;
                }
                else if ($("#userName").val().indexOf(' ') >= 0) {
                    //         alert("Enter User Name without spaces");
                    $("#userName").css("border", "1px solid red");
                    isUserNameVerify = false;
                }
                else {
                    $("#userName").css("border", "1px solid black");
                    isUserNameVerify = true;
                }
                var f = false;
                for (user in allUsers) {
                    if ($("#userName").val() == allUsers[user]["login"] && allUsers[user]["id"] != userData["ID"])
                        f = true;
                }
                if (f) {
                    $("#userName").css("border", "1px solid red");
                    isUserNameVerify = false;
                    alert("User Name already  exists");
                }
            }

            function checkName() {
                if ($("#Name").val().trim(' ') == 0) {
                    $("#Name").css("border", "1px solid red");
                    isNameVerify = false;
                }
                else {
                    $("#Name").css("border", "1px solid black");
                    isNameVerify = true;
                }
            }


            function validateEmail(emailvalue) {
                var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(emailvalue);
            }

            function checkEmail() {
                var f = false;
                for (user in allUsers) {
                    if ($("#email").val() == allUsers[user]["email"] && allUsers[user]["id"] != userData["ID"]) {
                        f = true;
                        break;
                    }
                }
                if (!validateEmail($("#email").val()) || f == true) {
                    //          alert("please  provide email");
                    $("#email").css("border", "1px solid red");
                    isEmailVerify = false;
                }
                else {
                    $("#email").css("border", "1px solid black");
                    isEmailVerify = true;
                }
            }

            function checkPassword() {
                if ($("#password").val().length == 0) {
                    //      alert("Please Enter password");
                    $("#password").css("border" , "1px solid red");
                    isPasswordVerify = false;
                }
                else {
                    $("#password").css("border" , "1px solid black");
                    isPasswordVerify = true;      //why  why error  if  remove this   ?
                }
            }

            function matchPassword() {
                if ($("#password").val() != $("#confirmPassword").val() || $("#password").val().length == 0) {
                    //  alert("Both Passwords  does not  match");
                    $("#password").css("border" , "1px solid red");
                    $("#confirmPassword").css("border" , "1px solid red");
                    isPasswordVerify = false;
                    $("#password").val($("#confirmPassword").val(''))

                }
                else {
                    $("#password").css("border" , "1px solid black");
                    $("#confirmPassword").css("border" , "1px solid black");
                    isPasswordVerify = true;
                }
            }
            function checkCountry() {

                if ($("#cmbCountries").val() == 0) {
                    $("#cmbCountries").css("border", "1px solid red");
                    isCountryVerify = false;
                }
                else {
                    $("#cmbCountries").css("border", "1px solid black");
                    isCountryVerify = true;
                }
            }

            function checkCity() {

                if ($("#cmbCities").val()== 0) {
                    $("#cmbCities").css("border", "1px solid red");
                    isCityVerify = false;
                }
                else {
                    $("#cmbCities").css("border", "1px solid black");
                    isCityVerify = true;
                }
            }


            $("#savebtn").on("click", function () {

                checkUserName();
                checkName();
                checkEmail();
                checkPassword();
                matchPassword();
                checkCountry();
                checkCity();

                if (isUserNameVerify &&
                    isNameVerify &&
                    isEmailVerify &&
                    isCountryVerify &&
                    isCityVerify &&
                    isPasswordVerify) {
                    userData["createdby"]=$("#adminid").val();
                    userData["userName"] = $("#userName").val();
                    userData["name"] =$("#Name").val();
                    userData["email"] = $("#email").val();
                    userData["password"] = $("#password").val();
                    userData["countryid"] = $("#cmbCountries").val();
                    userData["cityid"] = $("#cmbCities").val();
                    userData["isAdmin"] = $("#isAdmin").attr("checked") ? "Yes" : "No";

                    userData["act"] = "saveUser";
                    var setting = {};
                    setting.type = "Post";
                    setting.url = "api.php";
                    setting.dataType = "json";
                    setting.data = userData;
                    setting.success = function (u) {
                        alert("User Saved");
                        $(':input').val('');
                        tablefill();
                    };
                    setting.error = function () {
                        alert("Save user error");

                    }
                    $.ajax(setting);
                } else alert("Some value are missing");

            });
        }

    </script>
</head>
<body onload="Main()">
<div class="container-fluid">
    <div class="row form-group col-lg-4 col-lg-offset-2">
        <form>
            <legend align="center">User Management</legend>
            <div class="form-group">

                <input hidden name="adminid" id="adminid" value="<?php echo $adminid ?>">

                <input hidden name="userid" id="userid" value="<?php echo $id ?>">
                <label>UserName:*</label>
                <input type="text" id="userName" class="form-control">
            </div>
            <div class="form-group"><label>Name:*</label>
                <input type="text" id="Name" class="form-control">
            </div>
            <div class="form-group"><label>Email:*</label>
                <input type="email" id="email" class="form-control">
            </div>
            <div class="form-group">
                <label>Password:*</label>
                <input type="password" id="password" class="form-control">
            </div>
            <label>Confirm Password:</label>
            <input type="password" id="confirmPassword" class="form-control">
            <div class="form-group"><label>Country:</label>
                <select id="cmbCountries" class="form-control">

                </select>
            </div>
            <div class="form-group">
                <label>City</label>
                <select id="cmbCities" class="form-control">
                    <option>--select--</option>
                </select>
            </div>
            <div class="form form-group ">
                <label>IsAdmin</label> <input type="checkbox" id="isAdmin" name="isAdmin"
                                              class="checkbox checkbox-inline">
            </div>
            <div class="form-group">
                <input type="button" id="savebtn" value="Save" class="btn  btn-success col-lg-6">
                <input type="reset" value="Clear" class="btn  btn-warning col-lg-6">
            </div>
        </form>
    </div>
    <div class="col-lg-5 col-lg-offset-1">

        <table class="table" >
            <legend align="center">Users Table</legend>
            <tr>
                <th>ID</th>
                <th>userName</th>
                <th>Name</th>
                <th>Email</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
            <tbody id="userTable"></tbody>
        </table>
    </div>
</div>
</body>
</html>