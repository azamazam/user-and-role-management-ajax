<?php require('conn.php');
/**
 * Created by PhpStorm.
 * User: DeskTop
 * Date: 01-Apr-18
 * Time: 02:51 PM
 */
if ($_POST['act'] == "country") {
    $countries = [];
    $sql = "SELECT id,name FROM countries";

//Step-2: Execute SQL Query
    $result = mysqli_query($conn, $sql);

//Step-3: Get count of result
    $recordsFound = mysqli_num_rows($result);

//Step-4: Iterate row by row
    for ($i = 0; $i < $recordsFound; $i++) {
        $row = mysqli_fetch_assoc($result);

        //Step-5: Display values
        $countries[$i] = $row;


    }
    echo json_encode($countries);
} else if ($_POST['act'] == "city") {
    $cities = [];
    $countryid = $_POST["id"];
    $sql = "SELECT id,name FROM cities where countryid=$countryid";

//Step-2: Execute SQL Query
    $result = mysqli_query($conn, $sql);

//Step-3: Get count of result
    $recordsFound = mysqli_num_rows($result);

//Step-4: Iterate row by row
    for ($i = 0; $i < $recordsFound; $i++) {
        $row = mysqli_fetch_assoc($result);

        //Step-5: Display values
        $cities[$i] = $row;


    }
    echo json_encode($cities);
} else if ($_POST['act'] == "user") {
    $users = [];

    $sql = "SELECT id,login,name,email FROM users";

//Step-2: Execute SQL Query
    $result = mysqli_query($conn, $sql);

//Step-3: Get count of result
    $recordsFound = mysqli_num_rows($result);

//Step-4: Iterate row by row
    for ($i = 0; $i < $recordsFound; $i++) {
        $row = mysqli_fetch_assoc($result);

        //Step-5: Display values
        $users[$i] = $row;


    }
    echo json_encode($users);
} else if ($_POST['act'] == "saveUser") {
    $userName = $_POST['userName'];
    $name = $_POST['name'];
    $email = $_POST['email'];
    $countryid = $_POST['countryid'];
    $cityid=$_POST["cityid"];
    $isAdmin = $_POST['isAdmin'];
    $password = $_POST['password'];
    $createdby = $_POST["createdby"];
    $sql = "insert into users(login, password, name, email, countryid,cityid, isAdmin,createdby) VALUEs('$userName','$password','$name','$email','$countryid','$cityid','$isAdmin','$createdby'); ";

//Step-2: Execute SQL Query
    mysqli_query($conn, $sql);


    echo json_encode("successful");
}

else if ($_POST['act'] == "deleteUser") {
    $id = $_POST['id'];

    $sql = "delete from  users WHERE id='$id'";

//Step-2: Execute SQL Query
    mysqli_query($conn, $sql);

    echo json_encode("successful");
}
else if ($_POST['act'] == "role") {
    $roles = [];

    $sql = "SELECT roleid,name,description FROM roles";

//Step-2: Execute SQL Query
    $result = mysqli_query($conn, $sql);

//Step-3: Get count of result
    $recordsFound = mysqli_num_rows($result);

//Step-4: Iterate row by row
    for ($i = 0; $i < $recordsFound; $i++) {
        $row = mysqli_fetch_assoc($result);

        //Step-5: Display values
        $roles[$i] = $row;


    }
    echo json_encode($roles);
}
else if ($_POST['act'] == "saveRole") {
    $roleName = $_POST['roleName'];
    $description = $_POST['description'];
    $createdby = $_POST['createdby'];
    $sql = "insert into roles (name, description, createdby) VALUEs('$roleName','$description','$createdby'); ";

//Step-2: Execute SQL Query
    mysqli_query($conn, $sql);


    echo json_encode("successful");
}

else if ($_POST['act'] == "deleteRole") {
    $id = $_POST['id'];

    $sql = "delete from roles WHERE roleid='$id'";

//Step-2: Execute SQL Query
    mysqli_query($conn, $sql);

    echo json_encode("successful");
}

else if ($_POST['act'] == "permission") {
    $permissions = [];

    $sql = "SELECT permissionid,name,description FROM permissions";

//Step-2: Execute SQL Query
    $result = mysqli_query($conn, $sql);

//Step-3: Get count of result
    $recordsFound = mysqli_num_rows($result);

//Step-4: Iterate row by row
    for ($i = 0; $i < $recordsFound; $i++) {
        $row = mysqli_fetch_assoc($result);

        //Step-5: Display values
        $permissions[$i] = $row;


    }
    echo json_encode($permissions);
}
else if ($_POST['act'] == "savePermission") {
    $permissionName = $_POST['permissionName'];
    $description = $_POST['description'];
    $createdby = $_POST['createdby'];
    $sql = "insert into permissions (name, description, createdby) VALUEs('$permissionName','$description','$createdby'); ";

//Step-2: Execute SQL Query
    mysqli_query($conn, $sql);


    echo json_encode("successful");
}

else if ($_POST['act'] == "deletePermission") {
    $id = $_POST['id'];

    $sql = "delete from permissions WHERE permissionid='$id'";

//Step-2: Execute SQL Query
    mysqli_query($conn, $sql);

    echo json_encode("successful");
} else if ($_POST['act'] == "fillRolePermissionCB") {
    $data = [];
    $roles = [];
    $permissions = [];

    $sql = "SELECT permissionid,name FROM permissions";

    $result = mysqli_query($conn, $sql);

    $recordsFound = mysqli_num_rows($result);

    for ($i = 0; $i < $recordsFound; $i++) {
        $row = mysqli_fetch_assoc($result);
        $permissions[$i] = $row;
    }

    $sql = "SELECT roleid,name FROM roles";

    $result = mysqli_query($conn, $sql);

    $recordsFound = mysqli_num_rows($result);

    for ($i = 0; $i < $recordsFound; $i++) {
        $row = mysqli_fetch_assoc($result);
        $roles[$i] = $row;
    }
    $data[0] = $roles;
    $data[1] = $permissions;
    echo json_encode($data);
}

else if ($_POST['act'] == "saveRolePermission") {
    $roleid = $_POST['roleid'];
    $permissionid = $_POST['permissionid'];
    $sql = "insert into rolepermission (roleid,permissionid) VALUEs('$roleid','$permissionid'); ";

//Step-2: Execute SQL Query
    mysqli_query($conn, $sql);

    echo json_encode("successful");
}

if ($_POST['act'] == "rolePermission") {
    $rolePermissions = [];
    $sql = "SELECT rp.id id, r.name roleName, p.name permissionName from  rolepermission rp,roles r ,permissions p WHERE r.roleid=rp.roleid and p.permissionid=rp.permissionid";

//Step-2: Execute SQL Query
    $result = mysqli_query($conn, $sql);

//Step-3: Get count of result
    $recordsFound = mysqli_num_rows($result);

//Step-4: Iterate row by row
    for ($i = 0; $i < $recordsFound; $i++) {
        $row = mysqli_fetch_assoc($result);

        //Step-5: Display values
        $rolePermissions[$i] = $row;


    }
    echo json_encode($rolePermissions);
}
else if ($_POST['act'] == "saveRolePermission") {
    $roleid = $_POST['roleid'];
    $permissionid = $_POST['permissionid'];
    $sql = "insert into rolepermission (roleid,permissionid) VALUEs('$roleid','$permissionid'); ";

//Step-2: Execute SQL Query
    mysqli_query($conn, $sql);

    echo json_encode("successful");
}
else if ($_POST['act'] == "saveUserRole") {
    $userid = $_POST['userid'];
    $roleidid = $_POST['roleid'];
    $sql = "insert into userrole (roleid,userid) VALUEs('$roleid','$userid'); ";

//Step-2: Execute SQL Query
    mysqli_query($conn, $sql);

    echo json_encode("successful");
}

else if ($_POST['act'] == "userRole") {
    $userRoles = [];
    $sql = "SELECT ur.id id, u.login userName, r.name roleName from  userrole ur,roles r ,users u WHERE r.roleid=ur.roleid and ur.userid=u.id";

//Step-2: Execute SQL Query
    $result = mysqli_query($conn, $sql);

//Step-3: Get count of result
    $recordsFound = mysqli_num_rows($result);

//Step-4: Iterate row by row
    for ($i = 0; $i < $recordsFound; $i++) {
        $row = mysqli_fetch_assoc($result);

        //Step-5: Display values
        $userRoles[$i] = $row;

    }
    echo json_encode($rolePermissions);
}
?>