<?php include("navbar.php");
/**
 * Created by PhpStorm.
 * Role: DeskTop
 * Date: 3/9/2018
 * Time: 10:51 PM
 */


?>
<html>
<head>
    <script>


        function Main() {
            //      alert("Roles Permission Management ");
            var savebtn = document.getElementById("savebtn");
            var roleNamecmb = document.getElementById("roleName");
            var permissionNamecmb = document.getElementById("permissionName");
            var rolePermissionTable = document.getElementById("rolePermissionTable");

                 var roles,permissions;

            //    var rolesPermissions = SecurityManager.GetAllRolePermissions();

            var rolePermissionData = {};
            var isRoleNameVerify = true;
            var isPermissionNameVerify = true;
            var isRolePermissionVerify = true;

            roleNamecmb.onchange = checkRoleName;
            permissionNamecmb.onchange = checkPermissionName;


                tablefill();
            fillcomboboxes();

            function tablefill() {

                var setting = {};
                setting.type = "Post";
                setting.url = "api.php";
                setting.dataType = "json";
                setting.data = {"act": "rolePermission"};
                setting.success = function (rolesPermissions) {


                    for (rp in rolesPermissions) {
                        var row = document.createElement("tr");
                        rolePermissionTable.appendChild(row);

                        var datainrow = document.createElement("td");
                        datainrow.innerText = rolesPermissions[rp]["id"];
                        row.appendChild(datainrow);

                        datainrow = document.createElement("td");
                        datainrow.innerText = rolesPermissions[rp]["roleName"];
                        row.appendChild(datainrow);

                        datainrow = document.createElement("td");
                        datainrow.innerText = rolesPermissions[rp]["permissionName"];
                        row.appendChild(datainrow);


                        datainrow = document.createElement("td");
                        datainrow.innerHTML = "<a href='#' >edit</a>"
                        datainrow.setAttribute("onclick", "editRolePermission(" + rolesPermissions[rp]["ID"] + ");")
                        row.appendChild(datainrow);

                        datainrow = document.createElement("td");
                        datainrow.innerHTML = "<a href='#' >delete</a>"
                        datainrow.setAttribute("onclick", "deleteRolePermission(" + rolesPermissions[rp]["ID"] + ");")
                        row.appendChild(datainrow);
                    }
                }
                setting.error = function () {
                    alert("Load Table error");
                }
                $.ajax(setting);
            }

            function fillcomboboxes() {
                var setting = {};
                setting.type = "Post";
                setting.url = "api.php";
                setting.dataType = "json";
                setting.data = {"act": "fillRolePermissionCB"};
                setting.success = function (data) {
                  //  console.log(data);
                    roles=data[0];
                    permissions=data[1];
                    console.log(roles);
                    console.log (permissions);
                    for (role in  roles) {

                        $('#roleName').append($('<option>',
                            {
                                value: roles[role].roleid,
                                text: roles[role].name

                            }));
                    }
                    for (p in  permissions) {

                        $('#permissionName').append($('<option>',
                            {
                                value: permissions[p].permissionid,
                                text: permissions[p].name

                            }));
                    }
                }
                setting.error = function () {
                    alert("Load error");
                }
                $.ajax(setting);

            }


            function checkRoleName() {
                if (roleName.value == "--select--") {
                    //       alert("please  Enter role Name");
                    roleNamecmb.style.border = "1px solid red";
                    isRoleNameVerify = false;
                }
                else {
                    roleNamecmb.style.border = "1px solid black";
                    isRoleNameVerify = true;
                }

            }

            function checkPermissionName() {
                if (permissionNamecmb.value == "--select--") {
                    //       alert("please  Enter role Name");
                    permissionName.style.border = "1px solid red";
                    isPermissionNameVerify = false;
                }
                else {
                    permissionNamecmb.style.border = "1px solid black";
                    isPermissionNameVerify = true;
                }
            }

            function checkrolePermission() {
                isRolePermissionVerify = true;
                for (rp in rolesPermissions) {

                    if (rolesPermissions[rp].roleName == roleNamecmb.value &&
                        rolesPermissions[rp].permissionName == permissionNamecmb.value &&
                        rolesPermissions[rp].ID != rolePermissionData.ID) {

                        roleNamecmb.style.border = "1px solid red";
                        permissionNamecmb.style.border = "1px solid red";
                        isRolePermissionVerify = false;
                    }

                }
            }

            $("#savebtn").on("click", function () {

       //         checkRoleName();
        //        checkPermissionName();
        //        checkrolePermission();
                if (isRoleNameVerify && isPermissionNameVerify && isRolePermissionVerify) {
                    rolePermissionData["roleid"] =$("#roleName").val();
                    rolePermissionData["permissionid"] = $("#permissionName").val();


                    rolePermissionData["act"] = "saveRolePermission";
                    var setting = {};
                    setting.type = "Post";
                    setting.url = "api.php";
                    setting.dataType = "json";
                    setting.data = rolePermissionData;
                    setting.success = function (u) {
                        alert("Role Permission Saved");
                        $(':input').val('');
                    //    tablefill();
                    };
                    setting.error = function () {
                        alert("Save role error");

                    }
                    $.ajax(setting);
                } else alert("Some value are missing");

            });

            editRolePermission = function (roleId) {
                rolePermissionData = SecurityManager.GetRolePermissionById(roleId);
                roleNamecmb.value = rolePermissionData["roleName"];
                permissionNamecmb.value = rolePermissionData["permissionName"];

            }
            deleteRolePermission = function (roleId) {
           //     var isdelete = confirm("Are You sure  You want  to delete Role " + (SecurityManager.GetRolePermissionById(roleId))["roleName"]);
           //     if (isdelete)
              //      SecurityManager.DeleteRolePermission(roleId, loadPage, err);
            }


        }

    </script>
</head>
<body onload="Main();">
<div class="container-fluid">
    <div class="row form-group col-lg-4 col-lg-offset-2">
        <form>
            <legend align="center">Roles-Permission-Assignment</legend>
            <div class="form-group">
                <label>Role Name:*</label>
                <select id="roleName" class="form-control">
                    <option>--select--</option>
                </select>
            </div>
            <div class="form-group"><label>Permission:</label>
                <select id="permissionName" class="form-control">
                    <option>--select--</option>
                </select>
            </div>
            <div class="form-group">
                <input type="button" id="savebtn" value="Save" class="btn  btn-success col-lg-6">
                <input type="reset" value="Clear" class="btn  btn-warning col-lg-6">
            </div>
        </form>
    </div>
    <div class="col-lg-5 col-lg-offset-1">

        <table class="table" id="rolePermissionTable">
            <legend align="center">Role Permission Table</legend>
            <tr>
                <th>ID</th>
                <th>Role Name</th>
                <th>Permission</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
        </table>
    </div>
</div>
</body>
</html>

