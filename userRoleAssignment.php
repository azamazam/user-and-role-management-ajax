<?php include("navbar.php");
/**
 * Created by PhpStorm.
 * User: DeskTop
 * Date: 3/9/2018
 * Time: 10:51 PM
 */


?>
<html>
<head>
    <script>


        function Main() {
            //      alert("Users Role Management ");
            var savebtn = document.getElementById("savebtn");
            var userNamecmb = document.getElementById("userName");
            var roleNamecmb = document.getElementById("roleName");
            var userRoleTable = document.getElementById("userRoleTable");

            var users,roles;

            //    var usersRoles = SecurityManager.GetAllUserRoles();

            var userRoleData = {};
            var isUserNameVerify = true;
            var isRoleNameVerify = true;
            var isUserRoleVerify = true;

            userNamecmb.onchange = checkUserName;
            roleNamecmb.onchange = checkRoleName;


            tablefill();
            fillcomboboxes();

            function tablefill() {

                var setting = {};
                setting.type = "Post";
                setting.url = "api.php";
                setting.dataType = "json";
                setting.data = {"act": "userRole"};
                setting.success = function (usersRoles) {


                    for (rp in usersRoles) {
                        var row = document.createElement("tr");
                        userRoleTable.appendChild(row);

                        var datainrow = document.createElement("td");
                        datainrow.innerText = usersRoles[rp]["id"];
                        row.appendChild(datainrow);

                        datainrow = document.createElement("td");
                        datainrow.innerText = usersRoles[rp]["userName"];
                        row.appendChild(datainrow);

                        datainrow = document.createElement("td");
                        datainrow.innerText = usersRoles[rp]["roleName"];
                        row.appendChild(datainrow);


                        datainrow = document.createElement("td");
                        datainrow.innerHTML = "<a href='#' >edit</a>"
                        datainrow.setAttribute("onclick", "editUserRole(" + usersRoles[rp]["ID"] + ");")
                        row.appendChild(datainrow);

                        datainrow = document.createElement("td");
                        datainrow.innerHTML = "<a href='#' >delete</a>"
                        datainrow.setAttribute("onclick", "deleteUserRole(" + usersRoles[rp]["ID"] + ");")
                        row.appendChild(datainrow);
                    }
                }
                setting.error = function () {
                    alert("Load Table error");
                }
                $.ajax(setting);
            }

            function fillcomboboxes() {
                var setting = {};
                setting.type = "Post";
                setting.url = "api.php";
                setting.dataType = "json";
                setting.data = {"act": "fillUserRoleCB"};
                setting.success = function (data) {
                    //  console.log(data);
                    users=data[0];
                    roles=data[1];
                    console.log(users);
                    console.log (roles);
                    for (user in  users) {

                        $('#userName').append($('<option>',
                            {
                                value: users[user].userid,
                                text: users[user].name

                            }));
                    }
                    for (p in  roles) {

                        $('#roleName').append($('<option>',
                            {
                                value: roles[p].roleid,
                                text: roles[p].name

                            }));
                    }
                }
                setting.error = function () {
                    alert("Load error");
                }
                $.ajax(setting);

            }


            function checkUserName() {
                if (userName.value == "--select--") {
                    //       alert("please  Enter user Name");
                    userNamecmb.style.border = "1px solid red";
                    isUserNameVerify = false;
                }
                else {
                    userNamecmb.style.border = "1px solid black";
                    isUserNameVerify = true;
                }

            }

            function checkRoleName() {
                if (roleNamecmb.value == "--select--") {
                    //       alert("please  Enter user Name");
                    roleName.style.border = "1px solid red";
                    isRoleNameVerify = false;
                }
                else {
                    roleNamecmb.style.border = "1px solid black";
                    isRoleNameVerify = true;
                }
            }

            function checkuserRole() {
                isUserRoleVerify = true;
                for (rp in usersRoles) {

                    if (usersRoles[rp].userName == userNamecmb.value &&
                        usersRoles[rp].roleName == roleNamecmb.value &&
                        usersRoles[rp].ID != userRoleData.ID) {

                        userNamecmb.style.border = "1px solid red";
                        roleNamecmb.style.border = "1px solid red";
                        isUserRoleVerify = false;
                    }

                }
            }

            $("#savebtn").on("click", function () {

                //         checkUserName();
                //        checkRoleName();
                //        checkuserRole();
                if (isUserNameVerify && isRoleNameVerify && isUserRoleVerify) {
                    userRoleData["userid"] =$("#userName").val();
                    userRoleData["roleid"] = $("#roleName").val();


                    userRoleData["act"] = "saveUserRole";
                    var setting = {};
                    setting.type = "Post";
                    setting.url = "api.php";
                    setting.dataType = "json";
                    setting.data = userRoleData;
                    setting.success = function (u) {
                        alert("User Role Saved");
                        $(':input').val('');
                        //    tablefill();
                    };
                    setting.error = function () {
                        alert("Save user error");

                    }
                    $.ajax(setting);
                } else alert("Some value are missing");

            });

            editUserRole = function (userId) {
                userRoleData = SecurityManager.GetUserRoleById(userId);
                userNamecmb.value = userRoleData["userName"];
                roleNamecmb.value = userRoleData["roleName"];

            }
            deleteUserRole = function (userId) {
                //     var isdelete = confirm("Are You sure  You want  to delete User " + (SecurityManager.GetUserRoleById(userId))["userName"]);
                //     if (isdelete)
                //      SecurityManager.DeleteUserRole(userId, loadPage, err);
            }


        }

    </script>
</head>
<body onload="Main();">
<div class="container-fluid">
    <div class="row form-group col-lg-4 col-lg-offset-2">
        <form>
            <legend align="center">Users-Role-Assignment</legend>
            <div class="form-group">
                <label>User Name:*</label>
                <select id="userName" class="form-control">
                    <option>--select--</option>
                </select>
            </div>
            <div class="form-group"><label>Role:</label>
                <select id="roleName" class="form-control">
                    <option>--select--</option>
                </select>
            </div>
            <div class="form-group">
                <input type="button" id="savebtn" value="Save" class="btn  btn-success col-lg-6">
                <input type="reset" value="Clear" class="btn  btn-warning col-lg-6">
            </div>
        </form>
    </div>
    <div class="col-lg-5 col-lg-offset-1">

        <table class="table" id="userRoleTable">
            <legend align="center">User Role Table</legend>
            <tr>
                <th>ID</th>
                <th>User Name</th>
                <th>Role</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
        </table>
    </div>
</div>
</body>
</html>

